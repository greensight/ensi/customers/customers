<?php

return [
    'orders' => [
        'oms' => [
            'base_uri' => env('ORDERS_OMS_SERVICE_HOST') . "/api/v1",
        ],
    ],
    'customers' => [
        'customer-auth' => [
            'base_uri' => env('CUSTOMERS_CUSTOMER_AUTH_SERVICE_HOST') . "/api/v1",
        ],
    ],
    'logistic' => [
        'logistic' => [
            'base_uri' => env('LOGISTIC_LOGISTIC_SERVICE_HOST') . "/api/v1",
        ],
    ],
];
