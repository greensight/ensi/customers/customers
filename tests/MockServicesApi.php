<?php

namespace Tests;

use Ensi\CustomerAuthClient\Api\UsersApi as CustomerAuthUsersApi;
use Ensi\LogisticClient\Api\CargoOrdersApi;
use Ensi\LogisticClient\Api\DeliveryPricesApi;
use Ensi\LogisticClient\Api\DeliveryServicesApi;
use Ensi\LogisticClient\Api\GeosApi;
use Ensi\LogisticClient\Api\KpiApi;
use Ensi\OmsClient\Api\CommonApi as OmsCommonApi;
use Ensi\OmsClient\Api\DeliveriesApi;
use Ensi\OmsClient\Api\EnumsApi as OmsEnumsApi;
use Ensi\OmsClient\Api\OrdersApi;
use Ensi\OmsClient\Api\RefundsApi;
use Mockery\MockInterface;

trait MockServicesApi
{
    // =============== Customers ===============

    // region service Customers
    public function mockCustomersAuthUsersApi(): MockInterface|CustomerAuthUsersApi
    {
        return $this->mock(CustomerAuthUsersApi::class);
    }
    // endregion


    // =============== Logistic ===============

    // region service Logistic
    protected function mockLogisticCargoOrdersApi(): MockInterface|CargoOrdersApi
    {
        return $this->mock(CargoOrdersApi::class);
    }

    protected function mockLogisticKpiApi(): MockInterface|KpiApi
    {
        return $this->mock(KpiApi::class);
    }

    public function mockLogisticDeliveryServicesApi(): MockInterface|DeliveryServicesApi
    {
        return $this->mock(DeliveryServicesApi::class);
    }

    public function mockLogisticGeosApi(): MockInterface|GeosApi
    {
        return $this->mock(GeosApi::class);
    }

    public function mockLogisticDeliveryPricesApi(): MockInterface|DeliveryPricesApi
    {
        return $this->mock(DeliveryPricesApi::class);
    }
    // endregion


    // =============== Orders ===============

    // region service OMS
    public function mockOmsOrdersApi(): MockInterface|OrdersApi
    {
        return $this->mock(OrdersApi::class);
    }

    public function mockOmsDeliveriesApi(): MockInterface|DeliveriesApi
    {
        return $this->mock(DeliveriesApi::class);
    }

    public function mockOmsRefundsApi(): MockInterface|RefundsApi
    {
        return $this->mock(RefundsApi::class);
    }

    public function mockOmsEnumsApi(): MockInterface|OmsEnumsApi
    {
        return $this->mock(OmsEnumsApi::class);
    }

    public function mockOmsCommonApi(): MockInterface|OmsCommonApi
    {
        return $this->mock(OmsCommonApi::class);
    }
    // endregion
}
