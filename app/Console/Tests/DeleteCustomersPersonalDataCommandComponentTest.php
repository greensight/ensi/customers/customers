<?php

use App\Domain\Customers\Actions\Customers\DeletePersonalDataAction;
use App\Domain\Customers\Models\Customer;
use App\Domain\Customers\Models\DeletingCustomer;
use Ensi\LaravelTestFactories\FakerProvider;

use function Pest\Laravel\artisan;

use Tests\ComponentTestCase;

uses(ComponentTestCase::class)->group('component', 'console');

test('delete all customers that hang on the delete', function () {
    DeletingCustomer::factory()->count(2)->create();

    $this->mock(DeletePersonalDataAction::class)
        ->shouldReceive('execute')
        ->twice();

    artisan('customers:delete-personal-data --all')->assertSuccessful();
});

test('delete only one customer personal data that should be removed', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $customer = Customer::factory()->createOne(['delete_request' => now()]);

    $this->mock(DeletePersonalDataAction::class)
        ->shouldReceive('execute')
        ->once();

    artisan("customers:delete-personal-data --customer-id={$customer->id}")->assertSuccessful();
})->with(FakerProvider::$optionalDataset);

test('delete only one customer personal data that not should be removed', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $customer = Customer::factory()->createOne(['delete_request' => null]);

    $this->mock(DeletePersonalDataAction::class)->shouldNotReceive('execute');

    artisan("customers:delete-personal-data --customer-id={$customer->id}")->assertFailed();
})->with(FakerProvider::$optionalDataset);

test('without option', function () {
    $this->mock(DeletePersonalDataAction::class)->shouldNotReceive('execute');

    artisan('customers:delete-personal-data')->assertFailed();
});

test('both options', function () {
    $this->mock(DeletePersonalDataAction::class)->shouldNotReceive('execute');

    artisan('customers:delete-personal-data --all --customer-id=1')->assertFailed();
});
