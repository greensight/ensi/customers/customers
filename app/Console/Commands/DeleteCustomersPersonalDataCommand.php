<?php

namespace App\Console\Commands;

use App\Domain\Customers\Actions\Customers\DeleteAllDelayedCustomersPersonalDataAction;
use App\Domain\Customers\Actions\Customers\DeleteDelayedCustomerPersonalDataAction;
use App\Exceptions\IllegalOperationException;
use Illuminate\Console\Command;

class DeleteCustomersPersonalDataCommand extends Command
{
    protected $signature = 'customers:delete-personal-data
                            {--all : удалить персональные данные всех покупателей, которые на удалении}
                            {--customer-id= : удалить персональные данные указанного покупателя}';

    protected $description = 'Удаление всех персональных данных покупателей, которые висят на удалении';

    public function handle(
        DeleteDelayedCustomerPersonalDataAction $delayedCustomerPersonalDataAction,
        DeleteAllDelayedCustomersPersonalDataAction $allDelayedCustomersPersonalDataAction,
    ): int {
        $customerId = is_numeric($this->option('customer-id')) ? (int)$this->option('customer-id') : null;
        $deleteAll = $this->option('all');

        if (is_null($customerId) && !$deleteAll || !is_null($customerId) && $deleteAll) {
            $this->error("Необходимо указать одну из опций: --all или --customer-id");

            return self::INVALID;
        }

        try {
            if (is_null($customerId)) {
                $allDelayedCustomersPersonalDataAction->execute();
            } else {
                $delayedCustomerPersonalDataAction->execute($customerId);
            }
        } catch (IllegalOperationException $e) {
            $this->error($e->getMessage());

            return self::INVALID;
        }

        return self::SUCCESS;
    }
}
