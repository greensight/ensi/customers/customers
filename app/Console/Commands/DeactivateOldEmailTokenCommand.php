<?php

namespace App\Console\Commands;

use App\Domain\Customers\Actions\Customers\DeactivateEmailTokenAction;
use Illuminate\Console\Command;

class DeactivateOldEmailTokenCommand extends Command
{
    protected $signature = 'email:deactivate-token';
    protected $description = 'Деактивация токенов изменения почты, срок жизни которых истек';

    public function handle(DeactivateEmailTokenAction $action): void
    {
        $action->execute();
    }
}
