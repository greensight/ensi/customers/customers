<?php

namespace App\Providers;

use App\Domain\Customers\Models\Customer;
use App\Domain\Customers\Observers\CustomerObserver;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [];

    public function boot(): void
    {
        Customer::observe(CustomerObserver::class);
    }

    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
