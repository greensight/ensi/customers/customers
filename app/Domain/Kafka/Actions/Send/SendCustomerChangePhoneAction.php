<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Customers\Models\Customer;
use App\Domain\Kafka\Messages\Send\CustomerChangePhoneMessage;

class SendCustomerChangePhoneAction
{
    public function __construct(
        protected SendKafkaMessageAction $sendAction,
    ) {
    }

    public function execute(Customer $customer): void
    {
        $modelEvent = new CustomerChangePhoneMessage($customer);
        $this->sendAction->execute($modelEvent);
    }
}
