<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Customers\Models\Customer;
use App\Domain\Kafka\Messages\Send\CustomerChangeEmailMessage;

class SendCustomerChangeEmailAction
{
    public function __construct(
        protected SendKafkaMessageAction $sendAction,
    ) {
    }

    public function execute(Customer $customer): void
    {
        $modelEvent = new CustomerChangeEmailMessage($customer);
        $this->sendAction->execute($modelEvent);
    }
}
