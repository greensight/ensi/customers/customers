<?php

namespace App\Domain\Kafka\Actions\Send;

use App\Domain\Customers\Models\Customer;
use App\Domain\Kafka\Messages\Send\CustomerUpdatedEventMessage;

class SendCustomerUpdatedEventAction
{
    public function __construct(
        protected SendKafkaMessageAction $sendAction,
    ) {
    }

    public function execute(Customer $customer): void
    {
        $modelEvent = new CustomerUpdatedEventMessage($customer);
        $this->sendAction->execute($modelEvent);
    }
}
