<?php

use App\Domain\Customers\Models\Customer;
use App\Domain\Kafka\Actions\Send\SendCustomerChangePhoneAction;
use App\Domain\Kafka\Messages\Send\CustomerChangePhoneMessage;
use App\Domain\Kafka\Messages\Send\CustomerUpdatedEventMessage;
use Ensi\LaravelTestFactories\FakerProvider;

use function PHPUnit\Framework\assertEquals;
use function PHPUnit\Framework\assertIsArray;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('unit');

// region CustomerUpdatedEventMessage
test("generate CustomerUpdatedEventMessage success", function () {
    /** @var Customer $customer */
    $customer = Customer::factory()->create();

    $oldActive = $customer->first_name;
    $customer->first_name = 'test';

    $message = new CustomerUpdatedEventMessage($customer);
    $messageArray = $message->toArray();
    assertIsArray($messageArray);
    assertEquals($messageArray['old_data']['first_name'], $oldActive);
});
// endregion

test("generate CustomerChangePhoneMessage success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $this->mock(SendCustomerChangePhoneAction::class)->allows('execute');

    /** @var Customer $customer */
    $customer = Customer::factory()->create();

    $message = new CustomerChangePhoneMessage($customer);
    $messageArray = $message->toArray();

    assertIsArray($messageArray);
    assertEquals($messageArray['phone'], $customer->phone);
    assertEquals($messageArray['user_id'], $customer->user_id);
})->with(FakerProvider::$optionalDataset);
