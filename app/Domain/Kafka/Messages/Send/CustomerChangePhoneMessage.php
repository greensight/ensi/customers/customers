<?php

namespace App\Domain\Kafka\Messages\Send;

use App\Domain\Customers\Models\Customer;

class CustomerChangePhoneMessage extends KafkaMessage
{
    public function __construct(private readonly Customer $customer)
    {
    }

    public function toArray(): array
    {
        return [
            'user_id' => $this->customer->user_id,
            'phone' => $this->customer->phone,
        ];
    }

    public function topicKey(): string
    {
        return 'changes-phone';
    }
}
