<?php

namespace App\Domain\Kafka\Messages\Send;

use App\Domain\Customers\Models\Customer;

class CustomerUpdatedEventMessage extends KafkaMessage
{
    public function __construct(private Customer $customer)
    {
    }

    public function toArray(): array
    {
        $dirty = $this->customer->getDirty();
        $oldData = [];
        foreach ($dirty as $attributeKey => $attributeValue) {
            switch ($attributeKey) {
                case 'updated_at':
                    break;
                default:
                    $oldData[$attributeKey] = $this->customer->getOriginal($attributeKey);
            }
        }

        return [
            'customer_id' => $this->customer->id,
            'old_data' => $oldData,
        ];
    }

    public function topicKey(): string
    {
        return 'customer-updated';
    }
}
