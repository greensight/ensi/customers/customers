<?php

namespace App\Domain\Kafka\Messages\Send;

use App\Domain\Customers\Models\Customer;

class CustomerChangeEmailMessage extends KafkaMessage
{
    public function __construct(private readonly Customer $customer)
    {
    }

    public function toArray(): array
    {
        return [
            'customer_id' => $this->customer->id,
            'token' => $this->customer->email_token,
            'new_email' => $this->customer->new_email,
        ];
    }

    public function topicKey(): string
    {
        return 'changes-email';
    }
}
