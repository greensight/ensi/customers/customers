<?php

namespace App\Domain\Common\Tests\Factories\Orders;

use Ensi\LaravelTestFactories\BaseApiFactory;
use Ensi\OmsClient\Dto\CheckConditionsToDeleteCustomerPersonalDataResponse;
use Ensi\OmsClient\Dto\CheckConditionsToDeleteCustomerPersonalDataResponseData;

class CheckConditionsToDeleteCustomerPersonalDataFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        $canDelete = $this->faker->boolean();

        return [
            'can_delete' => $canDelete,
            'message' => $canDelete ? $this->faker->text() : null,
        ];
    }

    public function make(array $extra = []): CheckConditionsToDeleteCustomerPersonalDataResponseData
    {
        return new CheckConditionsToDeleteCustomerPersonalDataResponseData($this->makeArray($extra));
    }

    public function makeResponse(array $extra = []): CheckConditionsToDeleteCustomerPersonalDataResponse
    {
        return new CheckConditionsToDeleteCustomerPersonalDataResponse([
            'data' => $this->make($this->makeArray($extra)),
        ]);
    }
}
