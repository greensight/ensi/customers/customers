<?php

namespace App\Domain\Customers\Actions\Customers;

use App\Domain\Customers\Models\Customer;
use App\Domain\Customers\Models\DeletingCustomer;
use App\Exceptions\IllegalOperationException;
use Illuminate\Support\Collection;

class DeleteAllDelayedCustomersPersonalDataAction
{
    public function __construct(protected readonly DeletePersonalDataAction $deletePersonalDataAction)
    {
    }

    /**
     * @throws IllegalOperationException
     */
    public function execute(): void
    {
        $customersForDeleting = $this->loadCustomersForDeleting();

        foreach ($customersForDeleting as $customerForDeleting) {
            $this->deletePersonalDataAction->execute($customerForDeleting);
        }
    }

    /**
     * @return Collection<Customer>
     */
    protected function loadCustomersForDeleting(): Collection
    {
        $customerIds = DeletingCustomer::all()->pluck('customer_id')->toArray();

        return Customer::query()->whereIn('id', $customerIds)->get();
    }
}
