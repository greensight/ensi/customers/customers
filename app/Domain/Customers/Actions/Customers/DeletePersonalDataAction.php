<?php

namespace App\Domain\Customers\Actions\Customers;

use App\Domain\Customers\Models\Customer;
use App\Domain\Customers\Models\DeletingCustomer;
use Carbon\Carbon;
use Ensi\CustomerAuthClient\Api\CommonApi as UserCommonApi;
use Ensi\CustomerAuthClient\Dto\DeleteCustomerPersonalDataRequest as UserDeleteCustomerPersonalDataRequest;
use Ensi\LogisticClient\Api\CommonApi as LogisticCommonApi;
use Ensi\LogisticClient\Dto\CheckConditionsToDeleteCustomerPersonalDataRequest as CheckRequestInLogistic;
use Ensi\LogisticClient\Dto\CheckConditionsToDeleteCustomerPersonalDataResponse as CheckInLogisticResponse;
use Ensi\LogisticClient\Dto\DeleteCustomerPersonalDataRequest as LogisticDeleteCustomerPersonalDataRequest;
use Ensi\OmsClient\Api\CommonApi as OmsCommonApi;
use Ensi\OmsClient\Dto\CheckConditionsToDeleteCustomerPersonalDataRequest as CheckRequestInOms;
use Ensi\OmsClient\Dto\CheckConditionsToDeleteCustomerPersonalDataResponse as CheckInOmsResponse;
use Ensi\OmsClient\Dto\DeleteCustomerPersonalDataRequest as OmsDeleteCustomerPersonalDataRequest;
use GuzzleHttp\Promise\PromiseInterface;
use GuzzleHttp\Promise\Utils;
use Throwable;

class DeletePersonalDataAction
{
    protected array $errorDelete = [];
    protected bool $canDelete = true;

    public function __construct(
        protected readonly OmsCommonApi $omsCommonApi,
        protected readonly LogisticCommonApi $logisticCommonApi,
        protected readonly UserCommonApi $userCommonApi,
    ) {
    }

    public function execute(Customer|int $customer): void
    {
        if (is_int($customer)) {
            /** @var Customer $customer */
            $customer = Customer::query()->findOrFail($customer);
        }

        $this->setDeleteRequestTime($customer);

        if ($this->checkConditionsToDeletePersonalData($customer->id)) {
            $this->deletePersonalData($customer);
        } else {
            $this->addCustomerToDelayedDelete($customer->id);
        }

        $this->setErrorsWhenDeletingPersonalData($customer);

        $customer->saveQuietly();
    }

    protected function setDeleteRequestTime(Customer $customer): void
    {
        $customer->delete_request = Carbon::now();
    }

    protected function setErrorsWhenDeletingPersonalData(Customer $customer): void
    {
        $customer->error_delete = implode(' ', $this->errorDelete);
    }

    protected function addCustomerToDelayedDelete(int $customerId): void
    {
        DeletingCustomer::query()->firstOrCreate(['customer_id' => $customerId]);
    }

    protected function deleteCustomerFromDelayedDelete(int $customerId): void
    {
        DeletingCustomer::whereCustomerId($customerId)->delete();
    }

    protected function checkConditionsToDeletePersonalData(int $customerId): bool
    {
        try {
            $this->checkConditionsInOms($customerId);
            $this->checkConditionsInLogistic($customerId);

            Utils::all([
                $this->checkConditionsInOms($customerId),
                $this->checkConditionsInLogistic($customerId),
            ])->wait();

        } catch (Throwable $e) {
            $this->errorDelete[] = $e->getMessage();

            return false;
        }

        return $this->canDelete;
    }

    protected function checkConditionsInOms(int $customerId): PromiseInterface
    {
        $request = new CheckRequestInOms();
        $request->setCustomerId($customerId);

        return $this->omsCommonApi
            ->checkConditionsToDeleteCustomerPersonalDataAsync($request)
            ->then(function (CheckInOmsResponse $response) {
                $data = $response->getData();
                if ($data->getCanDelete()) {
                    return;
                }

                $this->errorDelete['oms'] = $data->getMessage();
                $this->canDelete = false;
            });
    }

    protected function checkConditionsInLogistic(int $customerId): PromiseInterface
    {
        $request = new CheckRequestInLogistic();
        $request->setCustomerId($customerId);

        return $this->logisticCommonApi
            ->checkConditionsToDeleteCustomerPersonalDataAsync($request)
            ->then(function (CheckInLogisticResponse $response) {
                $data = $response->getData();
                if ($data->getCanDelete()) {
                    return;
                }

                $this->errorDelete['logistics'] = $data->getMessage();
                $this->canDelete = false;
            });
    }

    protected function deletePersonalData(Customer$customer): void
    {
        try {
            $this->deleteCustomerData($customer);
            $this->deleteCustomerDataInAddresses($customer);

            $this->deleteCustomerDataInUsers($customer);
            $this->deleteCustomerDataInOms($customer);
            $this->deleteCustomerDataInLogistic($customer);

            Utils::all([
                $this->deleteCustomerDataInUsers($customer),
                $this->deleteCustomerDataInOms($customer),
                $this->deleteCustomerDataInLogistic($customer),
            ])->wait();

            $customer->is_deleted = true;
            $customer->save();

            $this->deleteCustomerFromDelayedDelete($customer->id);
        } catch (Throwable $e) {
            $this->errorDelete[] = $e->getMessage();
        }
    }

    protected function deleteCustomerData(Customer $customer): void
    {
        $customer->yandex_metric_id = null;
        $customer->google_analytics_id = null;
        $customer->first_name = null;
        $customer->last_name = null;
        $customer->middle_name = null;
        $customer->email = null;
        $customer->phone = null;
        $customer->gender = null;
        $customer->avatar = null;
        $customer->city = null;
        $customer->birthday = null;
        $customer->last_visit_date = null;
        $customer->comment_status = null;
        $customer->new_email = null;
        $customer->email_token = null;
        $customer->email_token_created_at = null;
        $customer->timezone = config('app.timezone');
        $customer->active = false;

        $customer->saveQuietly();
    }

    protected function deleteCustomerDataInAddresses(Customer $customer): void
    {
        $customer->load('addresses');

        foreach ($customer->addresses as $address) {
            $address->default = false;
            $address->address = null;

            $address->save();
        }
    }

    protected function deleteCustomerDataInUsers(Customer $customer): PromiseInterface
    {
        $request = new UserDeleteCustomerPersonalDataRequest();
        $request->setUserId($customer->user_id);

        return $this->userCommonApi->deleteCustomerPersonalDataAsync($request);
    }

    protected function deleteCustomerDataInOms(Customer $customer): PromiseInterface
    {
        $request = new OmsDeleteCustomerPersonalDataRequest();
        $request->setCustomerId($customer->id);

        return $this->omsCommonApi->deleteCustomerPersonalDataAsync($request);
    }

    protected function deleteCustomerDataInLogistic(Customer $customer): PromiseInterface
    {
        $request = new LogisticDeleteCustomerPersonalDataRequest();
        $request->setCustomerId($customer->id);

        return $this->logisticCommonApi->deleteCustomerPersonalDataAsync($request);
    }
}
