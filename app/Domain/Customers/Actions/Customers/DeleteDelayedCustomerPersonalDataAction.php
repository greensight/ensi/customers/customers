<?php

namespace App\Domain\Customers\Actions\Customers;

use App\Domain\Customers\Models\Customer;
use App\Exceptions\IllegalOperationException;

class DeleteDelayedCustomerPersonalDataAction
{
    public function __construct(protected readonly DeletePersonalDataAction $deletePersonalDataAction)
    {
    }

    /**
     * @throws IllegalOperationException
     */
    public function execute(int $customerId): void
    {
        /** @var Customer $customer */
        $customer = Customer::query()->findOrFail($customerId);

        if (blank($customer->delete_request)) {
            throw new IllegalOperationException('Покупатель не висит на удалении');
        }

        if (!$customer->is_deleted) {
            $this->deletePersonalDataAction->execute($customer);
        }
    }
}
