<?php

namespace App\Domain\Customers\Actions\Customers;

use App\Domain\Customers\Models\Customer;
use App\Domain\Kafka\Actions\Send\SendCustomerChangeEmailAction;
use App\Domain\Kafka\Actions\Send\SendCustomerChangePhoneAction;

class PatchCustomerAction
{
    public function __construct(
        protected SendCustomerChangeEmailAction $sendCustomerChangeEmailAction,
        protected SendCustomerChangePhoneAction $sendCustomerChangePhoneAction
    ) {
    }

    public function execute(int $customerId, array $fields): Customer
    {
        /** @var Customer $customer */
        $customer = Customer::query()->findOrFail($customerId);

        // Если пришел новый email, то отправляем письмо на подтверждение
        if (array_key_exists('email', $fields)) {
            $newEmail = $fields['email'];
            unset($fields['email']);
            $this->refreshToken($customer, $newEmail);
        }

        if (array_key_exists('phone', $fields)) {
            $this->sendCustomerChangePhoneAction->execute($customer);
        }

        $customer->update($fields);

        return $customer;
    }

    public function refreshToken(Customer $customer, string $newEmail): void
    {
        $customer->generateEmailToken($newEmail);
        $customer->save();

        $this->sendCustomerChangeEmailAction->execute($customer);
    }
}
