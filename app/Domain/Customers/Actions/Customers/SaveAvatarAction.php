<?php

namespace App\Domain\Customers\Actions\Customers;

use App\Domain\Customers\Models\Customer;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use RuntimeException;

class SaveAvatarAction
{
    public function __construct(protected EnsiFilesystemManager $fileManager)
    {
    }

    public function execute(int $customerId, UploadedFile $file): Customer
    {
        /** @var Customer $customer */
        $customer = Customer::query()->findOrFail($customerId);

        $hash = Str::random(20);
        $extension = $file->getClientOriginalExtension();
        $fileName = "avatar_{$customerId}_{$hash}.{$extension}";
        $hashedSubDirs = $this->fileManager->getHashedDirsForFileName($fileName);

        /** @var FilesystemAdapter $disk */
        $disk = Storage::disk($this->fileManager->publicDiskName());

        $path = $disk->putFileAs("avatars/{$hashedSubDirs}", $file, $fileName);
        if (!$path) {
            throw new RuntimeException("Unable to save file $fileName to directory avatars/{$hashedSubDirs}");
        }

        if ($customer->avatar) {
            $disk->delete($customer->avatar);
        }

        $customer->avatar = $path;
        $customer->save();

        return $customer;
    }
}
