<?php

namespace App\Domain\Customers\Actions\Customers;

use App\Domain\Customers\Models\Customer;

class CreateCustomerAction
{
    public function execute(array $fields): Customer
    {
        $customer = new Customer();
        $customer->fill($fields);

        $customer->save();

        return $customer;
    }
}
