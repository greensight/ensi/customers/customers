<?php

use App\Domain\Common\Tests\Factories\Logistic\CheckConditionsToDeleteCustomerPersonalDataFactory as CheckInLogisticResponse;
use App\Domain\Common\Tests\Factories\Orders\CheckConditionsToDeleteCustomerPersonalDataFactory as CheckInOmsResponse;
use App\Domain\Customers\Actions\Customers\DeletePersonalDataAction;
use App\Domain\Customers\Models\Address;
use App\Domain\Customers\Models\Customer;
use App\Domain\Customers\Models\DeletingCustomer;
use App\Domain\Kafka\Actions\Send\SendCustomerUpdatedEventAction;
use Ensi\CustomerAuthClient\Dto\EmptyDataResponse as CustomerAuthEmptyDataResponse;
use Ensi\LaravelTestFactories\FakerProvider;
use Ensi\LaravelTestFactories\PromiseFactory;
use Ensi\LogisticClient\Dto\EmptyDataResponse as LogisticEmptyDataResponse;
use Ensi\OmsClient\Dto\EmptyDataResponse as OmsEmptyDataResponse;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'customers');

test("DeletePersonalDataAction success all personal data deleted", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */

    $this->mock(SendCustomerUpdatedEventAction::class)->shouldReceive('execute');

    /** @var Customer $customer */
    $customer = Customer::factory()
        ->has(Address::factory()->state(function (array $attributes, Customer $customer) {
            return ['customer_id' => $customer->id];
        }))
        ->create();
    DeletingCustomer::factory()->create(['customer_id' => $customer->id]);

    $this->mockOmsCommonApi()->allows([
        'checkConditionsToDeleteCustomerPersonalDataAsync' => PromiseFactory::make(
            CheckInOmsResponse::new()->makeResponse(['can_delete' => true])
        ),
        'deleteCustomerPersonalDataAsync' => PromiseFactory::make(new OmsEmptyDataResponse()),
    ]);

    $this->mockLogisticCommonApi()->allows([
        'checkConditionsToDeleteCustomerPersonalDataAsync' => PromiseFactory::make(
            CheckInLogisticResponse::new()->makeResponse(['can_delete' => true])
        ),
        'deleteCustomerPersonalDataAsync' => PromiseFactory::make(new LogisticEmptyDataResponse()),
    ]);

    $this->mockCustomerUserCommonApi()->allows([
        'deleteCustomerPersonalDataAsync' => PromiseFactory::make(new CustomerAuthEmptyDataResponse()),
    ]);

    resolve(DeletePersonalDataAction::class)->execute($customer);

    assertDatabaseHas(Customer::class, [
        'id' => $customer->id,
        'is_deleted' => true,
        'active' => false,
        'email' => null,
        'phone' => null,
        'first_name' => null,
        'last_name' => null,
        'middle_name' => null,
    ]);

    assertDatabaseHas(Address::class, [
        'customer_id' => $customer->id,
        'address' => null,
    ]);

    assertDatabaseMissing(DeletingCustomer::class, [
        'customer_id' => $customer->id,
    ]);
})->with(FakerProvider::$optionalDataset);

test("DeletePersonalDataAction success add customer to delayed deleting", function (bool $omsCheckResult, bool $logisticCheckResult, ?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var IntegrationTestCase $this */

    $this->mock(SendCustomerUpdatedEventAction::class)->shouldReceive('execute');

    /** @var Customer $customer */
    $customer = Customer::factory()
        ->has(Address::factory()->state(function (array $attributes, Customer $customer) {
            return ['customer_id' => $customer->id];
        }))
        ->create();

    $this->mockOmsCommonApi()
        ->allows([
            'checkConditionsToDeleteCustomerPersonalDataAsync' => PromiseFactory::make(
                CheckInOmsResponse::new()->makeResponse(['can_delete' => $omsCheckResult])
            ),
        ])
        ->shouldNotReceive('deleteCustomerPersonalDataAsync');

    $this->mockLogisticCommonApi()
        ->allows([
            'checkConditionsToDeleteCustomerPersonalDataAsync' => PromiseFactory::make(
                CheckInLogisticResponse::new()->makeResponse(['can_delete' => $logisticCheckResult])
            ),
        ])
        ->shouldNotReceive('deleteCustomerPersonalDataAsync');

    $this->mockCustomerUserCommonApi()->shouldNotReceive('deleteCustomerPersonalAsync');

    resolve(DeletePersonalDataAction::class)->execute($customer);

    $this->assertNotNull(Customer::query()->findOrFail($customer->id)->delete_request);

    assertDatabaseHas(Customer::class, [
        'is_deleted' => false,
        'email' => $customer->email,
    ]);

    assertDatabaseHas(DeletingCustomer::class, [
        'customer_id' => $customer->id,
    ]);
})->with([
    'error in oms' => [false, true],
    'error in logistic' => [true, false],
], FakerProvider::$optionalDataset);
