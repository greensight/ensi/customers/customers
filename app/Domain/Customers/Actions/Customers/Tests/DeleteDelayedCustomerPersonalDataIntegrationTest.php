<?php

use App\Domain\Customers\Actions\Customers\DeleteDelayedCustomerPersonalDataAction;
use App\Domain\Customers\Actions\Customers\DeletePersonalDataAction;
use App\Domain\Customers\Models\Customer;
use App\Exceptions\IllegalOperationException;

use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'customers');

test("DeleteDelayedCustomerPersonalDataAction delete by customer_id success", function () {
    /** @var IntegrationTestCase $this */

    /** @var Customer $customer */
    $customer = Customer::factory()->create(['delete_request' => now()]);

    $this->mock(DeletePersonalDataAction::class)
        ->shouldReceive('execute')
        ->with(Mockery::on(function ($param) use ($customer) {
            return $param->is($customer);
        }))
        ->once();

    resolve(DeleteDelayedCustomerPersonalDataAction::class)->execute($customer->id);
});

test("DeleteDelayedCustomerPersonalDataAction delete by customer_id not marked", function () {
    /** @var IntegrationTestCase $this */

    /** @var Customer $customer */
    $customer = Customer::factory()->create(['delete_request' => null]);

    $this->mock(DeletePersonalDataAction::class)
        ->shouldNotReceive('execute');

    $action = resolve(DeleteDelayedCustomerPersonalDataAction::class);
    expect(fn () => $action->execute($customer->id))->toThrow(IllegalOperationException::class);
});
