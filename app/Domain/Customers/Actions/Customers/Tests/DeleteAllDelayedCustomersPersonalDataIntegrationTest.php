<?php

use App\Domain\Customers\Actions\Customers\DeleteAllDelayedCustomersPersonalDataAction;
use App\Domain\Customers\Actions\Customers\DeletePersonalDataAction;
use App\Domain\Customers\Models\Customer;
use App\Domain\Customers\Models\DeletingCustomer;
use Tests\IntegrationTestCase;

uses(IntegrationTestCase::class);
uses()->group('integration', 'customers');

test("DeleteAllDelayedCustomersPersonalDataAction delete all success", function () {
    /** @var IntegrationTestCase $this */

    // Ensure delete action is triggered if customer is marked for deletion
    /** @var Customer $customer */
    $customer = Customer::factory()->create();
    DeletingCustomer::factory()->create(['customer_id' => $customer->id]);

    // Ensure delete action is not triggered if customer isn't marked
    Customer::factory()->create();

    $this->mock(DeletePersonalDataAction::class)
        ->shouldReceive('execute')
        ->with(Mockery::on(function ($param) use ($customer) {
            return $param->is($customer);
        }))
        ->once();

    resolve(DeleteAllDelayedCustomersPersonalDataAction::class)->execute();
});
