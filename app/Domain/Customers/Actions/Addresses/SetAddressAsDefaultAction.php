<?php

namespace App\Domain\Customers\Actions\Addresses;

use App\Domain\Customers\Models\Address;

class SetAddressAsDefaultAction
{
    public function execute(int $addressId): void
    {
        /** @var Address $address */
        $address = Address::query()->findOrFail($addressId);
        if ($address->default) {
            return;
        }

        Address::query()
            ->where('customer_id', $address->customer_id)
            ->where('default', true)
            ->update(['default' => false]);

        $address->default = true;
        $address->save();
    }
}
