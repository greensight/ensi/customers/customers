<?php

namespace App\Domain\Customers\Actions\Addresses;

use App\Domain\Customers\Models\Address;

class ReplaceAddressAction
{
    public function execute(int $addressId, array $fields): Address
    {
        /** @var Address $address */
        $address = Address::query()->findOrFail($addressId);
        $address->customer_id = $fields['customer_id'];
        $address->address = [
            'address_string' => $fields['address']['address_string'],
            'post_index' => $fields['address']['post_index'] ?? "",
            'country_code' => $fields['address']['country_code'] ?? "",
            'region' => $fields['address']['region'] ?? "",
            'region_guid' => $fields['address']['region_guid'] ?? "",
            'area' => $fields['address']['area'] ?? "",
            'area_guid' => $fields['address']['area_guid'] ?? "",
            'city' => $fields['address']['city'] ?? "",
            'city_guid' => $fields['address']['city_guid'] ?? "",
            'street' => $fields['address']['street'] ?? "",
            'house' => $fields['address']['house'] ?? "",
            'block' => $fields['address']['block'] ?? "",
            'porch' => $fields['address']['porch'] ?? "",
            'intercom' => $fields['address']['intercom'] ?? "",
            'floor' => $fields['address']['floor'] ?? "",
            'flat' => $fields['address']['flat'] ?? "",
            'comment' => $fields['address']['comment'] ?? "",
            'geo_lat' => $fields['address']['geo_lat'] ?? "",
            'geo_lon' => $fields['address']['geo_lon'] ?? "",
        ];

        $address->save();

        return $address;
    }
}
