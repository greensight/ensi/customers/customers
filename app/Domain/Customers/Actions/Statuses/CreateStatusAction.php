<?php

namespace App\Domain\Customers\Actions\Statuses;

use App\Domain\Customers\Models\Status;

class CreateStatusAction
{
    public function execute(array $fields): Status
    {
        $status = new Status();
        $status->fill($fields);
        $status->save();

        return $status;
    }
}
