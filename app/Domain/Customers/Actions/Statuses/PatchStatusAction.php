<?php

namespace App\Domain\Customers\Actions\Statuses;

use App\Domain\Customers\Models\Status;

class PatchStatusAction
{
    public function execute(int $id, array $fields): Status
    {
        /** @var Status $status */
        $status = Status::query()->findOrFail($id);
        $status->fill($fields);
        $status->save();

        return $status;
    }
}
