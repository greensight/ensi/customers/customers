<?php

namespace App\Domain\Customers\Observers;

use App\Domain\Customers\Models\Customer;
use App\Domain\Kafka\Actions\Send\SendCustomerUpdatedEventAction;
use App\Exceptions\IllegalOperationException;

class CustomerObserver
{
    public function __construct(private SendCustomerUpdatedEventAction $sendCustomerUpdatedEventAction)
    {
    }

    public function saving(Customer $customer): void
    {
        if (($customer->phone === null) && !$customer->is_deleted) {
            throw new IllegalOperationException('Поле phone не может быть null');
        }
    }

    public function updated(Customer $customer): void
    {
        $this->sendCustomerUpdatedEventAction->execute($customer);
    }
}
