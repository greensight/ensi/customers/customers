<?php

namespace App\Domain\Customers\Models;

use App\Domain\Customers\Models\Tests\Factories\AddressFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

/**
 * Класс-модель для сущности "Адрес покупателя"
 *
 * @property int $id
 * @property int $customer_id - ид покупателя
 * @property array|null $address - информация об адресе
 * @property bool $default - адрес по умолчанию
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 */
class Address extends Model
{
    protected $table = 'customer_addresses';

    protected $casts = [
        'address' => 'array',
        'default' => 'boolean',
    ];

    public function customer(): BelongsTo
    {
        return $this->belongsTo(Customer::class);
    }

    public static function factory(): AddressFactory
    {
        return AddressFactory::new();
    }
}
