<?php

namespace App\Domain\Customers\Models\Tests\Factories;

use App\Domain\Customers\Models\Customer;
use App\Domain\Customers\Models\Status;
use App\Http\ApiV1\OpenApiGenerated\Enums\CustomerGenderEnum;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;
use Ensi\LaravelTestFactories\BaseModelFactory;

class CustomerFactory extends BaseModelFactory
{
    protected $model = Customer::class;

    public function definition(): array
    {
        return [
            'user_id' => $this->faker->modelId(),
            'status_id' => Status::factory(),
            'manager_id' => 1,
            'yandex_metric_id' => $this->faker->modelId(),
            'google_analytics_id' => $this->faker->modelId(),
            'active' => $this->faker->boolean(),
            'email' => $this->faker->unique()->email(),
            'phone' => $this->faker->unique()->numerify('+7##########'),
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'middle_name' => $this->faker->text(10),
            'gender' => $this->faker->randomEnum(CustomerGenderEnum::cases()),
            'create_by_admin' => $this->faker->boolean(),
            'city' => $this->faker->text(),
            'birthday' => $this->faker->date(),
            'last_visit_date' => $this->faker->date(),
            'comment_status' => $this->faker->text(),
            'timezone' => $this->faker->timezone(),
            'avatar' => null,
            'new_email' => $this->faker->unique()->email(),
            'is_deleted' => false,
            'error_delete' => $this->faker->nullable()->text(),
            'delete_request' => $this->faker->nullable()->date(BaseJsonResource::DATE_TIME_FORMAT),
        ];
    }

    public function withAvatar(?string $path = null): self
    {
        return $this->state(fn (array $attributes) => [
            'avatar' => $path ?: $this->faker->text(),
        ]);
    }
}
