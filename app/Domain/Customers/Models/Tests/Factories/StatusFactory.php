<?php

namespace App\Domain\Customers\Models\Tests\Factories;

use App\Domain\Customers\Models\Status;
use Ensi\LaravelTestFactories\BaseModelFactory;

class StatusFactory extends BaseModelFactory
{
    protected $model = Status::class;

    public function definition(): array
    {
        return [
            'name' => $this->faker->text(15),
        ];
    }
}
