<?php

namespace App\Domain\Customers\Models\Tests\Factories;

use App\Domain\Customers\Models\Customer;
use App\Domain\Customers\Models\DeletingCustomer;
use Ensi\LaravelTestFactories\BaseModelFactory;

class DeletingCustomerFactory extends BaseModelFactory
{
    protected $model = DeletingCustomer::class;

    public function definition(): array
    {
        return [
            'customer_id' => Customer::factory(),
        ];
    }
}
