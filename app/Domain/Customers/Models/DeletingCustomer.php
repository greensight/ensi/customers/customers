<?php

namespace App\Domain\Customers\Models;

use App\Domain\Customers\Models\Tests\Factories\DeletingCustomerFactory;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Класс-модель для удаления персональных данных покупателя
 *
 * @property int $id
 * @property int $customer_id - id покупателя
 *
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 *
 * @method static Builder|static whereCustomerId(int $value)
 */
class DeletingCustomer extends Model
{
    protected $fillable = ['customer_id'];

    protected $table = 'deleting_customers';

    public static function factory(): DeletingCustomerFactory
    {
        return DeletingCustomerFactory::new();
    }
}
