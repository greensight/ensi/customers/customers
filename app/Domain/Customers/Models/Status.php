<?php

namespace App\Domain\Customers\Models;

use App\Domain\Customers\Models\Tests\Factories\StatusFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $name
 */
class Status extends Model
{
    protected $table = 'statuses';

    protected $fillable = ['name'];

    public static function factory(): StatusFactory
    {
        return StatusFactory::new();
    }
}
