<?php

namespace App\Domain\Customers\Models;

use App\Domain\Customers\Models\Tests\Factories\CustomerFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\CustomerGenderEnum;
use Carbon\CarbonInterface;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Collection;

/**
 * Класс-модель для сущности "Покупатель"
 *
 * @property int $id
 * @property int $user_id - id покупателя в сервисе авторизации
 * @property int $status_id - id статуса пользователя
 * @property int $manager_id - id персонального менеджера
 * @property string|null $yandex_metric_id - Яндекс.Метрика (UserID)
 * @property string|null $google_analytics_id - Google Analytics (User ID)
 *
 * @property bool $active - Активность пользователя
 * @property string|null $email - Почта пользователя
 * @property string|null $phone - Номер телефона пользователя
 * @property string|null $first_name - Имя
 * @property string|null $last_name - Фамилия
 * @property string|null $middle_name - Отчество
 * @property CustomerGenderEnum|null $gender - Пол
 * @property bool $create_by_admin - Пользователь создан администратором
 * @property string|null $avatar - Объект с данными об аватаре
 * @property string|null $city - Город
 * @property CarbonInterface|null $birthday - День рождения
 * @property CarbonInterface|null $last_visit_date - Дата последнего визита
 * @property string|null $comment_status - Комментарий к статусу
 * @property string $timezone
 * @property string|null $new_email
 * @property string|null $email_token
 * @property CarbonInterface|null $email_token_created_at
 * @property bool $is_deleted - удалены ли данные пользователя
 * @property string|null $error_delete - ошибки при удалении персональных данных пользователя
 * @property CarbonInterface|null $delete_request - дата и время запроса на удаление
 *
 * @property CarbonInterface $created_at
 * @property CarbonInterface $updated_at
 *
 * @property-read string $full_name
 *
 * @property Collection<Address> $addresses
 * @property Status|null $status
 */
class Customer extends Model
{
    // Время жизни токена изменения почты в днях
    public const LIFE_TIME_EMAIL_TOKEN = 1;

    protected $table = 'customers';

    protected $attributes = [
        'is_deleted' => false,
    ];

    protected $fillable = [
        'user_id', 'status_id', 'manager_id', 'yandex_metric_id', 'google_analytics_id', 'active', 'email', 'phone',
        'first_name', 'last_name', 'middle_name', 'gender', 'create_by_admin', 'city', 'birthday',
        'last_visit_date', 'comment_status', 'timezone',
    ];

    protected $casts = [
        'active' => 'boolean',
        'create_by_admin' => 'boolean',
        'is_deleted' => 'boolean',
        'email_token_created_at' => 'datetime',
        'gender' => CustomerGenderEnum::class,
        'delete_request' => 'datetime',
        'birthday' => 'datetime',
        'last_visit_date' => 'datetime',
    ];

    public function addresses(): HasMany
    {
        return $this->hasMany(Address::class);
    }

    public function status(): HasOne
    {
        return $this->hasOne(Status::class, 'id', 'status_id');
    }

    public function generateEmailToken(string $newEmail): void
    {
        $this->new_email = $newEmail;
        $this->email_token = md5($this->email . time());
        $this->email_token_created_at = now();
    }

    public function destroyEmailToken(): void
    {
        $this->new_email = null;
        $this->email_token = null;
        $this->email_token_created_at = null;
    }

    public function getFullNameAttribute(): string
    {
        $pieces = [];
        if ($this->last_name) {
            $pieces[] = $this->last_name;
        }
        if ($this->first_name) {
            $pieces[] = $this->first_name;
        }
        if ($this->middle_name) {
            $pieces[] = $this->middle_name;
        }

        return implode(' ', $pieces);
    }

    public static function findByEmail(string $email): Customer
    {
        return Customer::where('email', $email)->first();
    }

    public static function findByPhone(string $phone): Customer
    {
        return Customer::where('phone', $phone)->first();
    }

    public static function factory(): CustomerFactory
    {
        return CustomerFactory::new();
    }
}
