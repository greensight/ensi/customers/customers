<?php

namespace App\Http\ApiV1\Modules\Customers\Resources;

use App\Domain\Customers\Models\Status;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin Status
 */
class StatusesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
        ];
    }
}
