<?php

namespace App\Http\ApiV1\Modules\Customers\Resources;

use App\Domain\Customers\Models\Customer;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin Customer
 */
class CustomersResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'user_id' => $this->user_id,
            'manager_id' => $this->manager_id,
            'yandex_metric_id' => $this->yandex_metric_id,
            'google_analytics_id' => $this->google_analytics_id,
            'status_id' => $this->status_id,
            'active' => $this->active,
            'email' => $this->email,
            'new_email' => $this->new_email,
            'email_token_created_at' => $this->email_token_created_at,
            'phone' => $this->phone,
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'middle_name' => $this->middle_name,
            'full_name' => $this->full_name,
            'gender' => $this->gender,
            'create_by_admin' => $this->create_by_admin,
            'avatar' => $this->mapPublicFileToResponse($this->avatar),
            'city' => $this->city,
            'birthday' => $this->dateToIso($this->birthday),
            'last_visit_date' => $this->last_visit_date,
            'comment_status' => $this->comment_status,
            'timezone' => $this->timezone,
            'is_deleted' => $this->is_deleted,
            'error_delete' => $this->error_delete,
            'delete_request' => $this->delete_request,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,

            'addresses' => AddressesResource::collection($this->whenLoaded('addresses')),
            'status' => new StatusesResource($this->whenLoaded('status')),
        ];
    }
}
