<?php

namespace App\Http\ApiV1\Modules\Customers\Resources;

use App\Domain\Customers\Models\Address;
use App\Http\ApiV1\Support\Resources\BaseJsonResource;

/**
 * @mixin Address
 */
class AddressesResource extends BaseJsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'default' => $this->default,
            'customer_id' => $this->customer_id,
            'address' => $this->address,
        ];
    }
}
