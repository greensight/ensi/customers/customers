<?php

namespace App\Http\ApiV1\Modules\Customers\Requests;

use App\Domain\Customers\Models\Customer;
use App\Http\ApiV1\Support\Requests\BaseFormRequest;
use Illuminate\Validation\Rule;

class CreateCustomerAddressRequest extends BaseFormRequest
{
    public function rules(): array
    {
        return [
            'customer_id' => ['required', Rule::exists(Customer::class, 'id')],
            'default' => ['boolean'],
            'address' => ['required', 'array'],
            'address.address_string' => ['required', 'string'],
            'address.post_index' => ['nullable', 'string'],
            'address.country_code' => ['nullable', 'string'],
            'address.region' => ['nullable', 'string'],
            'address.region_guid' => ['nullable', 'string'],
            'address.area' => ['nullable', 'string'],
            'address.area_guid' => ['nullable', 'string'],
            'address.city' => ['nullable', 'string'],
            'address.city_guid' => ['required', 'string'],
            'address.street' => ['nullable', 'string'],
            'address.house' => ['nullable', 'string'],
            'address.block' => ['nullable', 'string'],
            'address.porch' => ['nullable', 'string'],
            'address.intercom' => ['nullable', 'string'],
            'address.floor' => ['nullable', 'string'],
            'address.flat' => ['nullable', 'string'],
            'address.comment' => ['nullable', 'string'],
            'address.geo_lat' => ['nullable', 'numeric'],
            'address.geo_lon' => ['nullable', 'numeric'],
        ];
    }
}
