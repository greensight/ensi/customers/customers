<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use App\Domain\Customers\Models\Customer;
use Ensi\QueryBuilderHelpers\Filters\DateFilter;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class CustomersQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Customer::query());

        $this->allowedIncludes(['addresses', 'status']);

        $this->allowedSorts(
            [
                'id',
                'first_name',
                'last_name',
                'middle_name',
                'phone',
                'email',
                'created_at',
                'updated_at',
                'last_visit_date',
                'birthday',
                'active',
                'is_deleted',
            ]
        );

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            AllowedFilter::exact('user_id'),
            AllowedFilter::exact('manager_id'),
            AllowedFilter::exact('status_id'),
            AllowedFilter::exact('active'),
            AllowedFilter::exact('gender'),
            AllowedFilter::exact('create_by_admin'),

            ...StringFilter::make('phone')->exact()->contain(),
            ...StringFilter::make('email')->exact()->contain(),
            ...StringFilter::make('first_name')->contain(),
            ...StringFilter::make('last_name')->contain(),
            ...StringFilter::make('middle_name')->contain(),
            ...StringFilter::make('city')->contain(),

            ...DateFilter::make('created_at')->lte()->gte(),
            ...DateFilter::make('updated_at')->lte()->gte(),
            ...DateFilter::make('last_visit_date')->lte()->gte(),
            ...DateFilter::make('birthday')->lte()->gte(),
            AllowedFilter::exact('is_deleted'),
        ]);

        $this->defaultSort('id');
    }
}
