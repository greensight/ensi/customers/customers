<?php

namespace App\Http\ApiV1\Modules\Customers\Queries;

use App\Domain\Customers\Models\Status;
use Ensi\QueryBuilderHelpers\Filters\StringFilter;
use Spatie\QueryBuilder\AllowedFilter;
use Spatie\QueryBuilder\QueryBuilder;

class StatusQuery extends QueryBuilder
{
    public function __construct()
    {
        parent::__construct(Status::query());

        $this->allowedSorts(['id']);

        $this->allowedFilters([
            AllowedFilter::exact('id'),
            ...StringFilter::make('name')->contain(),
        ]);

        $this->defaultSort('id');
    }
}
