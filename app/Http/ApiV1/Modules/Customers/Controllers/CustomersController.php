<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\Customers\CreateCustomerAction;
use App\Domain\Customers\Actions\Customers\DeletePersonalDataAction;
use App\Domain\Customers\Actions\Customers\PatchCustomerAction;
use App\Domain\Customers\Actions\Customers\VerifyEmailAction;
use App\Http\ApiV1\Modules\Customers\Queries\CustomersQuery;
use App\Http\ApiV1\Modules\Customers\Requests\CreateCustomerRequest;
use App\Http\ApiV1\Modules\Customers\Requests\PatchCustomerRequest;
use App\Http\ApiV1\Modules\Customers\Requests\VerifyEmailCustomerRequest;
use App\Http\ApiV1\Modules\Customers\Resources\CustomersResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class CustomersController
{
    public function create(CreateCustomerRequest $request, CreateCustomerAction $action): CustomersResource
    {
        return new CustomersResource($action->execute($request->validated()));
    }

    public function get(int $customerId, CustomersQuery $query): CustomersResource
    {
        return new CustomersResource($query->findOrFail($customerId));
    }

    public function patch(int $customerId, PatchCustomerRequest $request, PatchCustomerAction $action): CustomersResource
    {
        return new CustomersResource($action->execute($customerId, $request->validated()));
    }

    public function deletePersonalData(int $customerId, DeletePersonalDataAction $action): EmptyResource
    {
        $action->execute($customerId);

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, CustomersQuery $query): AnonymousResourceCollection
    {
        return CustomersResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function searchOne(CustomersQuery $query): CustomersResource
    {
        return new CustomersResource($query->firstOrFail());
    }

    public function verifyEmail(VerifyEmailCustomerRequest $request, VerifyEmailAction $action): CustomersResource
    {
        return new CustomersResource($action->execute($request->getToken()));
    }
}
