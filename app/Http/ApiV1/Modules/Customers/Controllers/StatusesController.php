<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\Statuses\CreateStatusAction;
use App\Domain\Customers\Actions\Statuses\DeleteStatusAction;
use App\Domain\Customers\Actions\Statuses\PatchStatusAction;
use App\Http\ApiV1\Modules\Customers\Queries\StatusQuery;
use App\Http\ApiV1\Modules\Customers\Requests\CreateCustomerStatusRequest;
use App\Http\ApiV1\Modules\Customers\Requests\PatchCustomerStatusRequest;
use App\Http\ApiV1\Modules\Customers\Resources\StatusesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class StatusesController
{
    public function create(CreateCustomerStatusRequest $request, CreateStatusAction $action): StatusesResource
    {
        return new StatusesResource($action->execute($request->validated()));
    }

    public function get(int $statusId,  StatusQuery $query): ?StatusesResource
    {
        return new StatusesResource($query->findOrFail($statusId));
    }

    public function patch(int $statusId, PatchCustomerStatusRequest $request, PatchStatusAction $action): StatusesResource
    {
        return new StatusesResource($action->execute($statusId, $request->validated()));
    }

    public function delete(int $statusId, DeleteStatusAction $action): EmptyResource
    {
        $action->execute($statusId);

        return new EmptyResource();
    }

    public function search(PageBuilderFactory $pageBuilderFactory, StatusQuery $query): AnonymousResourceCollection
    {
        return StatusesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }
}
