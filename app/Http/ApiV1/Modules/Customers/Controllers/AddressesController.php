<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\Addresses\CreateAddressAction;
use App\Domain\Customers\Actions\Addresses\DeleteAddressAction;
use App\Domain\Customers\Actions\Addresses\ReplaceAddressAction;
use App\Domain\Customers\Actions\Addresses\SetAddressAsDefaultAction;
use App\Http\ApiV1\Modules\Customers\Queries\AddressesQuery;
use App\Http\ApiV1\Modules\Customers\Requests\CreateCustomerAddressRequest;
use App\Http\ApiV1\Modules\Customers\Requests\ReplaceCustomerAddressRequest;
use App\Http\ApiV1\Modules\Customers\Resources\AddressesResource;
use App\Http\ApiV1\Support\Pagination\PageBuilderFactory;
use App\Http\ApiV1\Support\Resources\EmptyResource;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class AddressesController
{
    public function create(CreateCustomerAddressRequest $request, CreateAddressAction $action): AddressesResource
    {
        return new AddressesResource($action->execute($request->validated()));
    }

    public function get(int $addressId, AddressesQuery $query): AddressesResource
    {
        return new AddressesResource($query->findOrFail($addressId));
    }

    public function replace(int $addressId, ReplaceCustomerAddressRequest $request, ReplaceAddressAction $action): AddressesResource
    {
        return new AddressesResource($action->execute($addressId, $request->validated()));
    }

    public function search(PageBuilderFactory $pageBuilderFactory, AddressesQuery $query): AnonymousResourceCollection
    {
        return AddressesResource::collectPage(
            $pageBuilderFactory->fromQuery($query)->build()
        );
    }

    public function delete(int $addressId, DeleteAddressAction $action): EmptyResource
    {
        $action->execute($addressId);

        return new EmptyResource();
    }

    public function setAsDefault(int $addressId, SetAddressAsDefaultAction $action): EmptyResource
    {
        $action->execute($addressId);

        return new EmptyResource();
    }
}
