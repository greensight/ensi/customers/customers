<?php

namespace App\Http\ApiV1\Modules\Customers\Controllers;

use App\Domain\Customers\Actions\Customers\DeleteAvatarAction;
use App\Domain\Customers\Actions\Customers\SaveAvatarAction;
use App\Http\ApiV1\Modules\Customers\Requests\UploadCustomerAvatarRequest;
use App\Http\ApiV1\Modules\Customers\Resources\CustomersResource;

class AvatarsController
{
    public function upload(int $customerId, UploadCustomerAvatarRequest $request, SaveAvatarAction $action): CustomersResource
    {
        return new CustomersResource($action->execute($customerId, $request->getFile()));
    }

    public function delete(int $customerId, DeleteAvatarAction $action): CustomersResource
    {
        return new CustomersResource($action->execute($customerId));
    }
}
