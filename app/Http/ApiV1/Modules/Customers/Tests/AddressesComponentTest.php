<?php


use App\Domain\Customers\Models\Address;
use App\Domain\Customers\Models\Customer;
use App\Http\ApiV1\Modules\Customers\Tests\Factories\Addresses\CreateAddressRequestFactory;
use App\Http\ApiV1\Modules\Customers\Tests\Factories\Addresses\ReplaceAddressRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Illuminate\Support\Collection;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\postJson;
use function Pest\Laravel\putJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/customers/addresses 201', function () {
    $request = CreateAddressRequestFactory::new()->make();

    postJson('/api/v1/customers/addresses', $request)
        ->assertCreated()
        ->assertJsonPath('data.customer_id', $request['customer_id']);

    assertDatabaseHas((new Address())->getTable(), [
        'customer_id' => $request['customer_id'],
    ]);
});

test('GET /api/v1/customers/addresses/{id} 200', function () {
    /** @var Address $address */
    $address = Address::factory()->create();

    getJson("/api/v1/customers/addresses/$address->id")
        ->assertOk()
        ->assertJsonPath('data.id', $address->id)
        ->assertJsonPath('data.default', $address->default);
});

test('GET /api/v1/customers/addresses/{id} 404', function () {
    getJson('/api/v1/customers/addresses/404')
        ->assertNotFound();
});

test('PUT /api/v1/customers/addresses/{id} 200', function () {
    /** @var Address $address */
    $address = Address::factory()->create();

    $request = ReplaceAddressRequestFactory::new()->make();

    putJson("/api/v1/customers/addresses/$address->id", $request)
        ->assertOk()
        ->assertJsonPath('data.id', $address->id);

    assertDatabaseHas((new Address())->getTable(), [
        'id' => $address->id,
        'customer_id' => $request['customer_id'],
    ]);
});

test('DELETE /api/v1/customers/addresses/{id} 200', function () {
    /** @var Address $addressOk */
    $addressOk = Address::factory()->create();
    /** @var Address $addressDelete */
    $addressDelete = Address::factory()->create();

    deleteJson("/api/v1/customers/addresses/$addressDelete->id")
        ->assertOk();

    assertDatabaseMissing((new Address())->getTable(), ['id' => $addressDelete->id]);
    assertDatabaseHas((new Address())->getTable(), ['id' => $addressOk->id]);
});

test('DELETE /api/v1/customers/addresses/{id} not existing 200', function () {
    deleteJson('/api/v1/customers/addresses/404')
        ->assertOk();
});

test('POST /api/v1/customers/addresses/{id}:set-as-default 200', function () {
    /** @var Customer $customer */
    $customer = Customer::factory()->create();

    /** @var Address $address */
    $address = Address::factory()->for($customer)->create(['default' => false]);
    /** @var Address $additionalAddress */
    $additionalAddress = Address::factory()->for($customer)->create(['default' => true]);

    postJson("/api/v1/customers/addresses/$address->id:set-as-default")
        ->assertOk();

    assertDatabaseHas((new Address())->getTable(), [
        'id' => $address->id,
        'default' => true,
    ]);

    assertDatabaseHas((new Address())->getTable(), [
        'id' => $additionalAddress->id,
        'default' => false,
    ]);
});

test('POST /api/v1/customers/addresses:search 200', function () {
    /** @var Customer $customerOne */
    $customerOne = Customer::factory()->create();
    /** @var Customer $customerTwo */
    $customerTwo = Customer::factory()->create();

    /** @var Collection<Address> $addresses */
    $addresses = Address::factory()
        ->count(10)
        ->sequence(
            ['customer_id' => $customerOne->id],
            ['customer_id' => $customerTwo->id],
        )
        ->create();

    $request = [
        'filter' => [
            'customer_id' => $customerTwo->id,
        ],
        'sort' => ['-id'],
    ];

    postJson('/api/v1/customers/addresses:search', $request)
        ->assertOk()
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.id', $addresses->last()->id)
        ->assertJsonPath('data.0.customer_id', $customerTwo->id);
});

test("POST /api/v1/customers/addresses:search filter 200", function (string $fieldKey, $value = null, ?string $filterKey = null, $filterValue = null) {
    /** @var Customer $customer */
    $customer = Customer::factory()->create();
    $value = $fieldKey == 'customer_id' ? $customer->id : $value;

    /** @var Address $address */
    $address = Address::factory()->create($value ? [$fieldKey => $value] : []);

    Address::factory()->create($fieldKey == 'default' ? ['default' => !$value] : []);

    postJson('/api/v1/customers/addresses:search', ['filter' => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $address->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $address->id);
})->with([
    ['customer_id', 1],
    ['default', true],
]);

test("POST /api/v1/customers/addresses:search sort success", function (string $sort) {
    Address::factory()->create();
    postJson("/api/v1/customers/addresses:search", ["sort" => [$sort]])->assertOk();
})->with([
    'id',
]);
