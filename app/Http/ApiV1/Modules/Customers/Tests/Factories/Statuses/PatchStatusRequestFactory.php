<?php

namespace App\Http\ApiV1\Modules\Customers\Tests\Factories\Statuses;

use Ensi\LaravelTestFactories\BaseApiFactory;

class PatchStatusRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'name' => $this->faker->text(),
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
