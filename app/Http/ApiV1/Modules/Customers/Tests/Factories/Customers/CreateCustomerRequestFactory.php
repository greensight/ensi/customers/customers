<?php

namespace App\Http\ApiV1\Modules\Customers\Tests\Factories\Customers;

use App\Domain\Customers\Models\Status;
use App\Http\ApiV1\OpenApiGenerated\Enums\CustomerGenderEnum;
use App\Http\ApiV1\Support\Tests\Factories\FileFactory;
use Ensi\LaravelTestFactories\BaseApiFactory;

class CreateCustomerRequestFactory extends BaseApiFactory
{
    public ?FileFactory $avatarFactory = null;

    protected function definition(): array
    {
        return [
            'user_id' => $this->faker->modelId(),
            'status_id' => Status::factory()->create()->id,
            'manager_id' => $this->faker->nullable()->modelId(),
            'yandex_metric_id' => $this->faker->nullable()->text(10),
            'google_analytics_id' => $this->faker->nullable()->text(10),
            'active' => $this->faker->boolean(),
            'email' => $this->faker->unique()->email(),
            'phone' => $this->faker->numerify('+7##########'),
            'first_name' => $this->faker->name(),
            'last_name' => $this->faker->lastName(),
            'middle_name' => $this->faker->nullable()->text(),
            'gender' => $this->faker->randomElement(CustomerGenderEnum::cases()),
            'create_by_admin' => $this->faker->boolean(),
            'city' => $this->faker->nullable()->city(),
            'birthday' => $this->faker->nullable()->date(),
            'comment_status' => $this->faker->nullable()->text(),
            'timezone' => $this->faker->timezone(),
            'avatar' => $this->avatarFactory?->make(),
            'new_email' => $this->faker->unique()->email(),
            'email_token' => $this->faker->unique()->text(15),
            'is_deleted' => false,
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }

    public function withAvatar(FileFactory $factory = null): self
    {
        return $this->immutableSet('avatarFactory', $factory ?? FileFactory::new());
    }
}
