<?php

namespace App\Http\ApiV1\Modules\Customers\Tests\Factories\Customers;

use App\Domain\Customers\Models\Status;
use App\Http\ApiV1\OpenApiGenerated\Enums\CustomerGenderEnum;
use Ensi\LaravelTestFactories\BaseApiFactory;

class PatchCustomerRequestFactory extends BaseApiFactory
{
    protected function definition(): array
    {
        return [
            'user_id' => $this->faker->modelId(),
            'manager_id' => $this->faker->optional()->numberBetween(1),
            'yandex_metric_id' => $this->faker->optional()->uuid,
            'google_analytics_id' => $this->faker->optional()->uuid,
            'email' => $this->faker->unique()->email,
            'phone' => $this->faker->unique()->numerify('+7##########'),
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'middle_name' => $this->faker->optional()->name,
            'gender' => $this->faker->optional()->randomElement(CustomerGenderEnum::cases()),
            'active' => $this->faker->boolean,
            'create_by_admin' => $this->faker->boolean,
            'birthday' => $this->faker->optional()->date,
            'comment_status' => $this->faker->optional()->sentence,
            'city' => $this->faker->optional()->city,
            'last_visit_date' => $this->faker->optional()->dateTime()?->format('Y-m-d\TH:i:s.u\Z'),
            'timezone' => $this->faker->timezone,

            'status_id' => $this->faker->boolean ? Status::factory()->create()->id : null,
        ];
    }

    public function make(array $extra = []): array
    {
        return $this->makeArray($extra);
    }
}
