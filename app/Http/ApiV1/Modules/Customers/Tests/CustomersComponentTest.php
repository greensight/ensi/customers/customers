<?php

use App\Domain\Common\Tests\Factories\Logistic\CheckConditionsToDeleteCustomerPersonalDataFactory as CheckInLogisticResponse;
use App\Domain\Common\Tests\Factories\Orders\CheckConditionsToDeleteCustomerPersonalDataFactory as CheckInOmsResponse;
use App\Domain\Customers\Actions\Customers\DeletePersonalDataAction;
use App\Domain\Customers\Models\Address;
use App\Domain\Customers\Models\Customer;
use App\Domain\Kafka\Actions\Send\SendCustomerUpdatedEventAction;
use App\Http\ApiV1\Modules\Customers\Tests\Factories\Customers\CreateCustomerRequestFactory;
use App\Http\ApiV1\Modules\Customers\Tests\Factories\Customers\PatchCustomerRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelTestFactories\FakerProvider;
use Ensi\LaravelTestFactories\PromiseFactory;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/customers/customers:verify-email', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $this->mock(SendCustomerUpdatedEventAction::class)->shouldReceive('execute');

    $customer = Customer::factory()->createOne();
    $customer->generateEmailToken($customer->new_email);
    $customer->save();
    $request = [
        'token' => $customer->email_token,
    ];

    assertDatabaseHas((new Customer())->getTable(), [
        'email_token' => $customer->email_token,
    ]);

    postJson('/api/v1/customers/customers:verify-email', $request)
        ->assertOk()
        ->assertJsonStructure(['data' => ['id', 'user_id', 'active', 'email', 'phone']])
        ->assertJsonPath('data.email', $customer->new_email);
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/customers/customers 201', function () {
    $request = CreateCustomerRequestFactory::new()->make();

    postJson('/api/v1/customers/customers', $request)
        ->assertCreated()
        ->assertJsonPath('data.user_id', $request['user_id']);

    assertDatabaseHas((new Customer())->getTable(), [
        'user_id' => $request['user_id'],
        'email' => $request['email'],
    ]);
});

test('POST /api/v1/customers/customers check email', function (string $email, int $status = 201) {
    $userData = CreateCustomerRequestFactory::new()->make([
        'email' => $email,
    ]);

    $response = postJson('/api/v1/customers/customers', $userData)
        ->assertStatus($status);

    if ($status == 400) {
        $response->assertJsonPath('errors.0.code', "ValidationError");
    }
})->with([
    ['example@domain.com'],
    ['user.name@domain.net'],
    ['user-name@domain.com'],
    ['user-name12@domain.com'],
    ['user-name@domain.ru'],

    ['user name@domain.com', 400],
    ['user-name@domaincom', 400],
    ['user-name@domain-com', 400],
    ['user-name@[127.0.0.1]', 400],
    ['   @domain.com', 400],
    ['user-name@@domain.com', 400],
    ['user..name@domain.com', 400],
    ['.username@domain.com', 400],
    ['username.@domain.com', 400],
    ['user-name@domain.com-', 400],
    ['user-name@-domain.com', 400],
]);

test('POST /api/v1/customers/customers:search 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $customers = Customer::factory()
        ->count(10)
        ->sequence(
            ['active' => true],
            ['active' => false],
        )
        ->create();

    postJson('/api/v1/customers/customers:search', [
        "sort" => ["-id"],
        "filter" => ["active" => true],
    ])
        ->assertOk()
        ->assertJsonCount(5, 'data')
        ->assertJsonPath('data.0.active', true);
})->with(FakerProvider::$optionalDataset);

test("POST /api/v1/customers/customers:search sort success", function (string $sort) {
    Customer::factory()->create();
    postJson("/api/v1/customers/customers:search", ["sort" => [$sort]])->assertOk();
})->with([
    'id',
    'first_name',
    'last_name',
    'middle_name',
    'phone',
    'email',
    'created_at',
    'updated_at',
    'last_visit_date',
    'birthday',
    'active',
    'is_deleted',
]);

test(
    'POST /api/v1/customers/customers:search filter success',
    function (string $fieldKey, $value = null, ?string $filterKey = null, $filterValue = null) {
        /** @var Customer $customer */
        $customer = Customer::factory()->create($value ? [$fieldKey => $value] : []);
        Customer::factory()->create();

        postJson('/api/v1/customers/customers:search', [
            'filter' => [($filterKey ?: $fieldKey) => ($filterValue ?: $customer->{$fieldKey})],
            'sort' => ['id'],
            'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1],
        ])
            ->assertOk()
            ->assertJsonCount(1, 'data')
            ->assertJsonPath('data.0.id', $customer->id);
    }
)->with([
    ['active', true],
    ['manager_id'],
    ['phone'],
    ['phone', '+79255553535', 'phone_like', '+7925555'],
    ['email', 'test@mail.ru', 'email_like', 'test'],
    ['gender'],
    ['create_by_admin'],
    ['first_name', 'Иван', 'first_name_like', 'Ив'],
    ['last_name', 'Петров', 'last_name_like', 'Петр'],
    ['middle_name', 'Валентинович', 'middle_name_like', 'Валентинович'],
    ['city', 'Москва', 'city_like', 'мос'],
    ['user_id', 10],
    ['created_at', now()->subDays(10), 'created_at_gte', now()->subMonth()],
    ['created_at', now(), 'created_at_lte', now()],
    ['updated_at', now()->subDays(10), 'updated_at_gte', now()->subMonth()],
    ['updated_at', now(), 'updated_at_lte', now()],
    ['last_visit_date', now()->subMonths(2), 'last_visit_date_gte', now()->subMonths(10)],
    ['last_visit_date', now(), 'last_visit_date_lte', now()],
    ['birthday', now()->subYears(15), 'birthday_gte', now()->subYears(20)],
    ['birthday', now(), 'birthday_lte', now()],
]);

test("POST /api/v1/customers/customers:search include success", function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Customer $model */
    $model = Customer::factory()->create();
    $addresses = Address::factory()->for($model)->count(3)->create();

    postJson("/api/v1/customers/customers:search", ["include" => [
        'addresses',
    ]])
        ->assertStatus(200)
        ->assertJsonCount($addresses->count(), 'data.0.addresses');
})->with(FakerProvider::$optionalDataset);

test('POST /api/v1/customers/customers:search-one success', function () {
    $customers = Customer::factory()
        ->count(5)
        ->create();

    postJson("/api/v1/customers/customers:search-one", [
        'include' => ['status'],
        'sort' => ['-id'],
    ])
        ->assertOk()
        ->assertJsonPath('data.status.id', $customers->last()->status_id);
});

test('POST /api/v1/customers/customers 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $request = CreateCustomerRequestFactory::new()->make();

    postJson('/api/v1/customers/customers', $request)
        ->assertStatus(201);

    assertDatabaseHas((new Customer())->getTable(), [
        'first_name' => $request['first_name'],
    ]);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/customers/customers/{id} 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Customer $model */
    $model = Customer::factory()->create();

    $request = PatchCustomerRequestFactory::new()->make();

    patchJson("/api/v1/customers/customers/{$model->id}", $request)
        ->assertJsonPath('data.first_name', $request['first_name'])
        ->assertStatus(200);

    assertDatabaseHas((new Customer())->getTable(), [
        'id' => $model->id,
        'first_name' => $request['first_name'],
    ]);
})->with(FakerProvider::$optionalDataset);

test('PATCH /api/v1/customers/customers/{id} 404', function () {
    $request = PatchCustomerRequestFactory::new()->make();

    $undefinedId = 1;
    patchJson("/api/v1/customers/customers/{$undefinedId}", $request)
        ->assertStatus(404);
});

test('GET /api/v1/customers/customers/{id} 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    /** @var Customer $model */
    $model = Customer::factory()->create();

    getJson("/api/v1/customers/customers/{$model->id}")
        ->assertJsonPath('data.first_name', $model->first_name)
        ->assertStatus(200);
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/customers/customers/{id} 404', function () {
    $undefinedId = 1;
    getJson("/api/v1/customers/customers/{$undefinedId}")
        ->assertStatus(404);
});

test('POST /api/v1/customers/customers/{id}:delete-personal-data 200', function (?bool $always) {
    FakerProvider::$optionalAlways = $always;

    $customer = Customer::factory()->create();

    $this->mock(DeletePersonalDataAction::class)
        ->shouldReceive('execute')
        ->withArgs([$customer->id])
        ->once();

    postJson("/api/v1/customers/customers/{$customer->id}:delete-personal-data")
        ->assertStatus(200);
})->with(FakerProvider::$optionalDataset);

test('GET /api/v1/customers/customers:search-one 200', function () {
    /** @var Customer $customer */
    $customer = Customer::factory()->create();

    postJson('/api/v1/customers/customers:search-one', ['filter' => ['id' => $customer->id]])
        ->assertOk()
        ->assertJsonPath('data.id', $customer->id)
        ->assertJsonPath('data.email', $customer->email);
});

test('GET /api/v1/customers/customers:delete-personal-data 200', function () {
    $this->mockOmsCommonApi()->allows([
        'checkConditionsToDeleteCustomerPersonalDataAsync' => PromiseFactory::make(
            CheckInOmsResponse::new()->makeResponse(['can_delete' => true])
        ),
    ]);
    $this->mockLogisticCommonApi()->allows([
        'checkConditionsToDeleteCustomerPersonalDataAsync' => PromiseFactory::make(
            CheckInLogisticResponse::new()->makeResponse(['can_delete' => true])
        ),
    ]);

    /** @var Customer $customer */
    $customer = Customer::factory()->create();

    postJson("/api/v1/customers/customers/$customer->id:delete-personal-data")
        ->assertOk();

    assertDatabaseHas((new Customer())->getTable(), [
        'id' => $customer->id,
        'email' => null,
        'phone' => null,
        'active' => false,
    ]);
});
