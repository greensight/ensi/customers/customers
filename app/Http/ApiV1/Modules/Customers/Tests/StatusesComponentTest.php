<?php

use App\Domain\Customers\Models\Status;
use App\Http\ApiV1\Modules\Customers\Tests\Factories\Statuses\CreateStatusRequestFactory;
use App\Http\ApiV1\Modules\Customers\Tests\Factories\Statuses\PatchStatusRequestFactory;
use App\Http\ApiV1\OpenApiGenerated\Enums\PaginationTypeEnum;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertModelMissing;
use function Pest\Laravel\deleteJson;
use function Pest\Laravel\getJson;
use function Pest\Laravel\patchJson;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/customers/statuses:search 200', function () {
    $status = Status::factory()
        ->count(10)
        ->create();

    postJson('/api/v1/customers/statuses:search', [
        'sort' => ['-id'],
    ])
        ->assertOk()
        ->assertJsonCount(10, 'data')
        ->assertJsonPath('data.0.id', $status->last()->id);
});

test('POST /api/v1/customers/statuses:search filter success', function (string $fieldKey, $value = null, ?string $filterKey = null, $filterValue = null) {
    /** @var Status $status */
    $status = Status::factory()->create($value ? [$fieldKey => $value] : []);
    Status::factory()->create();

    postJson("/api/v1/customers/statuses:search", ['filter' => [
        ($filterKey ?: $fieldKey) => ($filterValue ?: $status->{$fieldKey}),
    ], 'sort' => ['id'], 'pagination' => ['type' => PaginationTypeEnum::OFFSET, 'limit' => 1]])
        ->assertOk()
        ->assertJsonCount(1, 'data')
        ->assertJsonPath('data.0.id', $status->id);
})->with([
    ['id'],
    ['name', 'order_my', 'name_like', '_my'],
]);

test('POST /api/v1/customers/statuses:search sort success', function (string $sort) {
    Status::factory()->create();
    postJson('/api/v1/customers/statuses:search', ['sort' => [$sort]])->assertOk();
})->with([
    'id',
]);

test('POST /api/v1/customers/statuses 201', function () {
    $request = CreateStatusRequestFactory::new()->make();

    postJson('/api/v1/customers/statuses', $request)
        ->assertCreated();

    assertDatabaseHas((new Status())->getTable(), [
        'name' => $request['name'],
    ]);
});

test('GET /api/v1/customers/statuses/{id} 200', function () {
    /** @var Status $model */
    $model = Status::factory()->create();

    getJson("/api/v1/customers/statuses/$model->id")
        ->assertJsonPath('data.name', $model->name)
        ->assertOk();
});

test('GET /api/v1/customers/statuses/{id} 404', function () {
    $undefinedId = 1;
    getJson("/api/v1/customers/statuses/$undefinedId")
        ->assertNotFound();
});

test('DELETE /api/v1/customers/statuses/{id} 200', function () {
    /** @var Status $model */
    $model = Status::factory()->create();

    deleteJson("/api/v1/customers/statuses/$model->id")
        ->assertOk();

    assertModelMissing($model);
});

test('DELETE /api/v1/customers/statuses/{id} undefined', function () {
    $undefinedId = 1;
    deleteJson("/api/v1/customers/statuses/$undefinedId")
        ->assertOk();
});

test('PATCH /api/v1/customers/statuses/{id} 200', function () {
    /** @var Status $model */
    $model = Status::factory()->create();

    $request = PatchStatusRequestFactory::new()->make();

    patchJson("/api/v1/customers/statuses/$model->id", $request)
        ->assertJsonPath('data.name', $request['name'])
        ->assertOk();

    assertDatabaseHas((new Status())->getTable(), [
        'id' => $model->id,
        'name' => $request['name'],
    ]);
});

test('PATCH /api/v1/customers/statuses/{id} 404', function () {
    $request = PatchStatusRequestFactory::new()->make();

    $undefinedId = 1;
    patchJson("/api/v1/customers/statuses/$undefinedId", $request)
        ->assertNotFound();
});
