<?php

use App\Domain\Customers\Models\Customer;
use App\Domain\Kafka\Actions\Send\SendCustomerUpdatedEventAction;
use App\Http\ApiV1\Support\Tests\ApiV1ComponentTestCase;
use Ensi\LaravelEnsiFilesystem\EnsiFilesystemManager;
use Illuminate\Filesystem\FilesystemAdapter;
use Illuminate\Http\UploadedFile;

use function Pest\Laravel\assertDatabaseHas;
use function Pest\Laravel\assertDatabaseMissing;
use function Pest\Laravel\postJson;

uses(ApiV1ComponentTestCase::class);
uses()->group('component');

test('POST /api/v1/customers/customers/{id}:upload-avatar success', function () {
    $fs = resolve(EnsiFilesystemManager::class);
    Storage::fake($fs->publicDiskName());

    $customer = Customer::factory()->create();

    $file = UploadedFile::fake()->create('123.png', kilobytes: 20);

    postFile("/api/v1/customers/customers/{$customer->id}:upload-avatar", $file)
        ->assertStatus(200)
        ->assertJsonStructure(['data' => ['id', 'avatar' => ['path', 'root_path', 'url']]])
        ->assertJsonPath('data.id', $customer->id);

    $customer->refresh();

    /** @var FilesystemAdapter $disk */
    $disk = Storage::disk($fs->publicDiskName());
    $disk->assertExists($customer->avatar);
});

test('POST /api/v1/customers/customers/{id}:upload-avatar with exists avatar success', function () {
    $fs = resolve(EnsiFilesystemManager::class);
    Storage::fake($fs->publicDiskName());

    $customer = Customer::factory()->withAvatar()->create();

    // old avatar file
    /** @var FilesystemAdapter $disk */
    $disk = Storage::disk($fs->publicDiskName());

    $disk->write($customer->avatar, '123');

    // new avatar
    $uploadFile = UploadedFile::fake()->create('123.png', kilobytes: 20);

    postFile("/api/v1/customers/customers/{$customer->id}:upload-avatar", $uploadFile)
        ->assertStatus(200)
        ->assertJsonStructure(['data' => ['id', 'avatar' => ['path', 'root_path', 'url']]])
        ->assertJsonPath('data.id', $customer->id);

    /** @var FilesystemAdapter $disk */
    $disk = Storage::disk($fs->publicDiskName());
    $disk->assertMissing($customer->avatar); // old avatar missing
    $disk->assertExists($customer->refresh()->avatar); // new avatar exists
});

test('POST /api/v1/customers/customers/{id}:delete-avatar success', function () {
    $fs = resolve(EnsiFilesystemManager::class);
    Storage::fake($fs->publicDiskName());

    $customer = Customer::factory()->withAvatar()->create();

    // old avatar file
    /** @var FilesystemAdapter $disk */
    $disk = Storage::disk($fs->publicDiskName());
    $disk->write($customer->avatar, '123');

    postJson("/api/v1/customers/customers/{$customer->id}:delete-avatar")
        ->assertStatus(200)
        ->assertJsonPath('data.id', $customer->id)
        ->assertJsonPath('data.avatar', null);

    /** @var FilesystemAdapter $disk */
    $disk = Storage::disk($fs->publicDiskName());
    $disk->assertMissing($customer->avatar);
});

test('POST /api/v1/customers/customers/{id}:upload-avatar and delete-avatar 200', function () {
    $this->mock(SendCustomerUpdatedEventAction::class)->shouldReceive('execute');

    /** @var Customer $customer */
    $customer = Customer::factory()->create();

    $fs = resolve(EnsiFilesystemManager::class);
    Storage::fake($fs->publicDiskName());

    $file = UploadedFile::fake()->image('123.png', 300, 300);

    $avatarPath = postFile("/api/v1/customers/customers/$customer->id:upload-avatar", $file)
        ->assertOk()
        ->assertJsonStructure([
            'data' => [
                'id',
                'avatar' => ['path', 'root_path', 'url'],
            ],
        ])
        ->json('data.avatar.path');

    assertDatabaseHas((new Customer())->getTable(), ['id' => $customer->id, 'avatar' => $avatarPath]);

    postJson("/api/v1/customers/customers/$customer->id:delete-avatar")
        ->assertOk()
        ->assertJsonPath('data.avatar', null);

    assertDatabaseMissing((new Customer())->getTable(), ['id' => $customer->id, 'avatar' => $avatarPath]);
});
