# Ensi Customers

Профили покупателей

## Разворот локально

Общие шаги разворота сервиса на локальной машине описаны в отдельном документе в [Gitlab Pages](https://ensi-platform.gitlab.io/docs/tech/back)

## Зависимости

| Название | Описание                                                                                                                                                                                                                                                                                 | Переменные окружения |
|---|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---|
| PostgreSQL | Основная БД сервиса                                                                                                                                                                                                                                                                      | DB_CONNECTION<br/>DB_HOST<br/>DB_PORT<br/>DB_DATABASE<br/>DB_USERNAME<br/>DB_PASSWORD |
| Kafka | Брокер сообщений. <br/>Producer осуществляет запись в следующие топики:<br/> - `<контур>.customers.fact.customer-updated.1`<br/> - `<контур>.customers.fact.changes-email.1`<br/> - `<контур>.customers.fact.changes-phone.1`<br/> Consumer (слушатель) в данном сервисе не используется | KAFKA_CONTOUR<br/>KAFKA_BROKER_LIST<br/>KAFKA_SECURITY_PROTOCOL<br/>KAFKA_SASL_MECHANISMS<br/>KAFKA_SASL_USERNAME<br/>KAFKA_SASL_PASSWORD<br/> |

## Лицензия

[Открытая лицензия на право использования программы для ЭВМ Greensight Ecom Platform (GEP)](LICENSE.md).
