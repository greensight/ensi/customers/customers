<?php

use App\Domain\Customers\Models\Customer;
use App\Domain\Customers\Models\DeletingCustomer;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class () extends Migration {
    public function up(): void
    {
        Schema::table('deleting_customers', function (Blueprint $table) {
            $this->prepareTable();
            $table->foreign('customer_id')
                ->references('id')
                ->on('customers')
                ->cascadeOnDelete();
        });
    }

    protected function prepareTable(): void
    {
        $customerIds = Customer::all()->pluck('id')->toArray();
        DeletingCustomer::query()->whereNotIn('customer_id', $customerIds)->delete();
    }

    public function down(): void
    {
        Schema::table('deleting_customers', function (Blueprint $table) {
            $table->dropForeign('deleting_customers_customer_id_foreign');
        });
    }
};
